# Kisi-kisi

## Soal
Pertimbangan anda dalam memilih membuat prosedur atau function
## Jawaban
- Reusability
- Readability
- Hiding details
- Menghindari duplikasi kode

## Soal
Debugging pemograman PHP
## Jawaban
Ubah konfigurasi php.ini menjadi :
````
error_reporting = E_ALL
display_errors = On
display_startup_errors = On
track_errors = On
````

Print data dengan:
````php
// Cetak variabel
print_r($data);
// Exit script
die;

// Menampilkan informasi terstruktur tentang suatu variabel
var_dump($variable);
// Exit script
die;
````

## Soal
Pengguna basis data
## Jawaban
- End User / Casual User
- Database Administrator
- Programmer
- Specialized / Sophisticated User

## Soal
Debugging MySQL syntax:
## Jawaban
Using PHP :
````php
$link = mysql_connect("localhost", "root", "");

if (mysql_select_db("blog_db", $link))
{
  // Untuk kode error
  echo mysql_errno($link);
  // Untuk pesan error
  echo mysql_error($link);
}
````

Using MySQL :
````sql
/*
  Menampilkan error yang berasal dari statement yang di execute.
*/
SHOW ERRORS
SHOW WARNINGS
````

## Soal
Langkah kerja untuk melakukan perhitungan rata rata dari 3 nilai ?
## Jawaban
````php
// Asumsikan mengirim form menggunakan method post
// Bilangan 1 masuk variabel bilangan_1, dan seterusnya

$sum = $_POST['bilangan_1'] + $_POST['bilangan_2'] + $_POST['bilangan_3'];

$avg = $sum / 3;

echo "Rata rata 3 bilangan adalah: " . $avg;  
````

## Soal
Kebutuhan , dan penggunaan tools debugging
## Jawab
Debugging tools (disebut debugger) digunakan untuk mengidentifikasi kesalahan pengkodean pada berbagai tahap pengembangan. 

Mereka digunakan untuk mereproduksi kondisi di mana kesalahan telah terjadi, kemudian memeriksa status program pada saat itu dan menemukan penyebabnya. 

Pemrogram dapat melacak eksekusi program langkah demi langkah dengan mengevaluasi nilai variabel dan menghentikan eksekusi di mana pun diperlukan untuk mendapatkan nilai variabel atau mengatur ulang variabel program.

Tools debugging:
- PHP - Xdebug