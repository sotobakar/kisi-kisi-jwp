<?php

function hitung_nilai_akhir($nilai_uts, $nilai_uas)
{
  return ($nilai_uts * 0.4) + ($nilai_uas * 0.6);
}

function hitung_nilai_indeks($nilai_akhir)
{
  if ($nilai_akhir >= 80) {
    return "A";
  } elseif ($nilai_akhir >= 70) {
    return "B";
  } elseif ($nilai_akhir >= 50) {
    return "C";
  } elseif ($nilai_akhir >= 40) {
    return "D";
  } elseif ($nilai_akhir < 40) {
    return "Gagal";
  }
}


// Masukkan nilai uts dan uas ke variabel
$nilai_uts = $_POST["nilai_uts"];
$nilai_uas = $_POST["nilai_uas"];

// Cek apakah input berbentuk angka
if (!is_numeric($nilai_uts) || !is_numeric($nilai_uas)) {
  echo "Tolong input nilai dalam bentuk angka.";
} else {
  $nilai_akhir = hitung_nilai_akhir($nilai_uts, $nilai_uas);
  $nilai_indeks = hitung_nilai_indeks($nilai_akhir);

  echo "Nilai UTS anda    : " . $nilai_uts;
  echo "<br>";
  echo "Nilai UAS anda    : " . $nilai_uas;
  echo "<br>";
  echo "Nilai akhir anda  : " . $nilai_akhir;
  echo "<br>";
  echo "Nilai indeks anda  : " . $nilai_indeks;
}
